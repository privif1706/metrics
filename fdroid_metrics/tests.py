import unittest

import fdroid_metrics
import glob
import inspect
import json
import os
import tempfile
import sys
from datetime import datetime, timedelta, timezone


class FdroidMetricsTest(unittest.TestCase):
    def setUp(self):
        self.basedir = os.path.realpath(
            os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), '..')
        )
        self.testfiles = os.path.abspath(os.path.join(self.basedir, '.testfiles'))
        if not os.path.exists(self.testfiles):
            os.makedirs(self.testfiles)

    def test_prepare_metrics_data_repo_error(self):
        prefix = inspect.currentframe().f_code.co_name
        with tempfile.TemporaryDirectory(prefix, dir=self.testfiles) as tmpdir:
            os.chdir(tmpdir)
            open('example.com', 'w').close()
            with self.assertRaises(Exception):
                fdroid_metrics.prepare_metrics_data_repo('example.com')

    def test_prepare_metrics_data_repo_no_git_remote(self):
        prefix = inspect.currentframe().f_code.co_name
        with tempfile.TemporaryDirectory(prefix, dir=self.testfiles) as tmpdir:
            os.chdir(tmpdir)
            with self.assertRaises(Exception):
                fdroid_metrics.prepare_metrics_data_repo('thisdoesnotexist.com')

    @unittest.skipIf('CI' in os.environ, 'requires SSH Key')
    def test_get_metrics_data_repo(self):
        """test getting the git repo from the domain

        It is too much of a pain to manage the SSH key for this job,
        so it is skipped in CI.

        """
        import git

        prefix = inspect.currentframe().f_code.co_name
        with tempfile.TemporaryDirectory(prefix, dir=self.testfiles) as tmpdir:
            os.chdir(tmpdir)
            domain = 'mirror.f-droid.org'
            self.assertFalse(os.path.exists(domain))
            fdroid_metrics.get_metrics_data_repo(domain)
            self.assertTrue(os.path.join(domain, '.git'))
            git_dir = os.path.join(tmpdir, domain)
            git_repo = git.Repo(domain)
            git_repo.git.reset('--hard', '1c926987')
            fdroid_metrics.pull_commits(git_repo)

    def test_commit_compiled_json(self):
        import git

        prefix = inspect.currentframe().f_code.co_name
        with tempfile.TemporaryDirectory(prefix, dir=self.testfiles) as tmpdir:
            os.chdir(tmpdir)
            domain = 'gitrepo.foo'
            os.mkdir(domain)
            git_repo = git.Repo.init(domain)
            os.chdir(domain)
            readme = 'README.md'
            with open(readme, 'w') as fp:
                fp.write('README')
            git_repo.index.add([readme])
            git_repo.index.commit('add README')
            self.assertEqual(1, len(list(git_repo.iter_commits('master'))))

            fdroid_metrics.commit_compiled_json(git_repo)  # nothing to add
            self.assertEqual(1, len(list(git_repo.iter_commits('master'))))

            with open('2020-10-20.json', 'w') as fp:
                fp.write('{}')
            fdroid_metrics.commit_compiled_json(git_repo)
            self.assertEqual(2, len(list(git_repo.iter_commits('master'))))

            open('2020-01-01.json', 'w').close()
            fdroid_metrics.commit_compiled_json(git_repo)  # ignore 0 size files
            self.assertEqual(2, len(list(git_repo.iter_commits('master'))))

            with open('2020-10-27.json', 'w') as fp:
                fp.write('{"2020-10-27": "fake"}')
            fdroid_metrics.commit_compiled_json(git_repo)
            self.assertEqual(3, len(list(git_repo.iter_commits('master'))))

    def test_push_commits(self):
        import git

        prefix = inspect.currentframe().f_code.co_name
        with tempfile.TemporaryDirectory(prefix, dir=self.testfiles) as tmpdir:
            os.chdir(tmpdir)
            domain = 'gitrepo.foo'
            git_remote = os.path.join(tmpdir, 'remote', domain)
            os.makedirs(git_remote)
            git_remote_repo = git.Repo.init(git_remote)

            fdroid_metrics.REMOTE_URL_FORMAT = os.path.join(
                tmpdir, 'remote', '{domain}'
            )
            git_repo = fdroid_metrics.get_metrics_data_repo(domain)
            with open(os.path.join(domain, '2020-10-20.json'), 'w') as fp:
                fp.write('{}')
            fdroid_metrics.commit_compiled_json(git_repo)
            self.assertEqual(1, len(list(git_repo.iter_commits('master'))))

            remote_commit_glob = os.path.join(
                git_remote_repo.git_dir, 'objects', '[0-9a-f][0-9a-f]', '[0-9a-f]*'
            )
            self.assertEqual(0, len(glob.glob(remote_commit_glob)))
            fdroid_metrics.push_commits(git_repo)
            self.assertEqual(3, len(glob.glob(remote_commit_glob)))

    @unittest.skipUnless(
        sys.version_info.major > 3 or sys.version_info.minor > 6,
        '%G and %V were added in Python 3.6, datetime.fromisoformat() was added in Python 3.7',
    )
    def test_get_week_start_date(self):
        """test date normalizing against Python 3.6's %G %V"""
        for y in range(1000, 3000):
            for d in range(1, 4):
                dt = datetime.fromisoformat('%04d-01-0%d 12:34:56+05:00' % (y, d))
                normalized36 = datetime.strptime(
                    dt.strftime('%G %V 1'), '%G %V %w'
                ).replace(tzinfo=timezone.utc)
                self.assertEqual(fdroid_metrics.get_week_start_date(dt), normalized36)

    @unittest.skipUnless(
        sys.version_info.major > 3 or sys.version_info.minor > 6,
        'datetime.fromisoformat() was added in Python 3.7',
    )
    def test_get_week_filter(self):
        """Filter out dates from the current week, since it is incomplete"""
        now = datetime.now(timezone.utc)
        incomplete_week_start = fdroid_metrics.get_week_start_date(now)
        self.assertTrue(now - incomplete_week_start < timedelta(days=7))
        for d in range(1, 32):
            now = datetime.fromisoformat('2021-03-%02d 12:34:56+05:00' % (d))
            incomplete_week_start = fdroid_metrics.get_week_start_date(now)
            self.assertTrue(now - incomplete_week_start < timedelta(days=7))

    def test_render_index_json(self):
        prefix = inspect.currentframe().f_code.co_name
        with tempfile.TemporaryDirectory(prefix, dir=self.testfiles) as tmpdir:
            os.chdir(tmpdir)
            domain = 'example.com'
            os.mkdir(domain)
            outdir = 'public'
            os.mkdir(outdir)
            os.chdir(outdir)
            open(fdroid_metrics.get_hit_bucket_file_name(domain, '1'), 'w').close()
            open(fdroid_metrics.get_hit_bucket_file_name(domain, '0'), 'w').close()
            open(fdroid_metrics.get_hit_bucket_file_name(domain, '2'), 'w').close()
            fdroid_metrics.render_index_json(domain)
            indexfile = os.path.join(
                tmpdir, outdir, fdroid_metrics.get_hit_bucket_file_name(domain, 'index')
            )
            with open(indexfile) as fp:
                data = json.load(fp)
            self.assertEqual(['2.json', '1.json', '0.json'], data)

    def test_render_index_html(self):
        prefix = inspect.currentframe().f_code.co_name
        with tempfile.TemporaryDirectory(prefix, dir=self.testfiles) as tmpdir:
            os.chdir(tmpdir)
            domain0 = 'example.com'
            domain1 = 'foobar.com'
            outputdir = 'public'
            os.makedirs(os.path.join(tmpdir, outputdir, domain0))
            os.makedirs(os.path.join(tmpdir, outputdir, domain1))
            os.chdir(outputdir)
            open(os.path.join(domain0, 'index.json'), 'w').close()
            open(os.path.join(domain1, 'index.json'), 'w').close()
            outputfile = os.path.join(tmpdir, outputdir, 'index.html')
            self.assertFalse(os.path.exists(outputfile))
            fdroid_metrics.render_index_html([domain0, domain1])
            with open(outputfile) as fp:
                contents = fp.read()
                self.assertTrue(domain0 in contents)
                self.assertTrue(domain1 in contents)

    def test_load_hit_bucket_json(self):
        prefix = inspect.currentframe().f_code.co_name
        with tempfile.TemporaryDirectory(prefix, dir=self.testfiles) as tmpdir:
            os.chdir(tmpdir)
            d = {
                'hitsPerCountry': {'US', 'AT', 'ZH'},
                'weekStartDate': fdroid_metrics.get_week_start_date(datetime.now()),
            }
            f = 'test.json'
            with open(f, 'w') as fp:
                json.dump(d, fp, cls=fdroid_metrics.Encoder)
            data = fdroid_metrics.load_hit_bucket_json(f)
            self.assertTrue(isinstance(data['weekStartDate'], datetime))

    def test_Encoder(self):
        dt = datetime.fromtimestamp(1611139095)
        self.assertIsNone(dt.tzinfo)
        d = {
            'hitsPerCountry': {'US', 'AT', 'ZH'},
            'weekStartDate': fdroid_metrics.get_week_start_date(dt),
        }
        output = json.loads(json.dumps(d, cls=fdroid_metrics.Encoder))
        self.assertEqual(['AT', 'US', 'ZH'], output['hitsPerCountry'])
        self.assertEqual('2021-01-18T00:00:00+00:00', output['weekStartDate'])
        if sys.version_info.major > 3 or sys.version_info.minor > 6:
            self.assertIsNotNone(datetime.fromisoformat(output['weekStartDate']).tzinfo)


if __name__ == '__main__':
    unittest.main()
